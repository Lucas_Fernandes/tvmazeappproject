package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.repository.EpisodeRepository
import com.repository.tvmazeapp.view.fragment.interaction.EpisodeInteraction
import com.repository.tvmazeapp.view.fragment.state.EpisodeState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class EpisodesViewModel @Inject constructor(
    private val episodeRepository: EpisodeRepository,
) : ViewModel() {

    val state: StateFlow<EpisodeState>
        get() = _state
    private val _state: MutableStateFlow<EpisodeState> = MutableStateFlow(EpisodeState.Empty)

    fun interact(interaction: EpisodeInteraction) {
        when (interaction) {
            EpisodeInteraction.Empty -> {}
            is EpisodeInteraction.GetEpisodes -> getEpisodes(interaction.id)
        }
    }

    private fun getEpisodes(id: Int) {
        viewModelScope.launch {
            _state.value = EpisodeState.Loading
            val request = episodeRepository.getListOfEpisodes(id)
            if (request.isSuccessful) {
                request.body()?.let { _state.value = EpisodeState.Success(it) }
            } else {
                _state.value = EpisodeState.Error
            }
        }
    }
}