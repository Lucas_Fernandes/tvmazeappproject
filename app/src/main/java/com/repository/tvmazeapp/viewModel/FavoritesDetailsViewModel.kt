package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote
import com.repository.tvmazeapp.repository.FavoritesRepository
import com.repository.tvmazeapp.repository.TvShowsDetailsRepository
import com.repository.tvmazeapp.view.fragment.interaction.FavoritesDetailsInteraction
import com.repository.tvmazeapp.view.fragment.state.FavoritesDetailsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoritesDetailsViewModel @Inject constructor(
    private val tvShowsDetailsRepository: TvShowsDetailsRepository,
    private val favoritesRepository: FavoritesRepository
) : ViewModel() {

    val state: StateFlow<FavoritesDetailsState>
        get() = _state
    private val _state = MutableStateFlow<FavoritesDetailsState>(FavoritesDetailsState.Empty)

    private lateinit var lastRequestResult: TvShowDetailsRemote

    fun interact(interaction: FavoritesDetailsInteraction) {
        when (interaction) {
            FavoritesDetailsInteraction.Empty -> { }
            is FavoritesDetailsInteraction.GetTvShowsDetails ->
                getTvShowDetails(interaction.seasonId)
            FavoritesDetailsInteraction.DeleteShowFromFavorites ->
                deleteShowFromFavorites(lastRequestResult)
        }
    }

    private fun deleteShowFromFavorites(lastRequestResult: TvShowDetailsRemote) {
        viewModelScope.launch(Dispatchers.IO) {
            val favoriteEntity = lastRequestResult.convertToDatabaseEntity(isFavorite = false)
            favoriteEntity?.let { favoritesRepository.delete(it) }
        }
    }

    private fun getTvShowDetails(seasonId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _state.value = FavoritesDetailsState.Loading
            val requestTvShowsDetails = tvShowsDetailsRepository.getTvShowDetails(seasonId)
            if (requestTvShowsDetails.isSuccessful) {
                requestTvShowsDetails.body()?.let { lastRequestResult = it }
                getListOfEpisodeAndHandleResults(seasonId, lastRequestResult)
            } else {
                _state.value = FavoritesDetailsState.Error
            }
        }
    }

    private suspend fun getListOfEpisodeAndHandleResults(
        seasonId: Int,
        tvShowDetails: TvShowDetailsRemote
    ) {
        val listOfSeasonsRequest = tvShowsDetailsRepository.getSeasons(seasonId)
        if (listOfSeasonsRequest.isSuccessful) {
            _state.apply {
                value = FavoritesDetailsState.TvShowsSuccess(tvShowDetails)
                value = FavoritesDetailsState.ListOfSeasonsSuccess(listOfSeasonsRequest.body())
                value = FavoritesDetailsState.Empty
            }
        } else {
            _state.apply {
                value = FavoritesDetailsState.Error
                value = FavoritesDetailsState.Empty
            }
        }
    }

}