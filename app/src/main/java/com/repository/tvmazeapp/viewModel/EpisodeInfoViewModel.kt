package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.repository.EpisodeInfoRepository
import com.repository.tvmazeapp.view.fragment.interaction.EpisodeInfoInteraction
import com.repository.tvmazeapp.view.fragment.state.EpisodeInfoState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class EpisodeInfoViewModel @Inject constructor(
    private val repository: EpisodeInfoRepository
) : ViewModel() {

    val state: StateFlow<EpisodeInfoState>
        get() = _state
    private val _state = MutableStateFlow<EpisodeInfoState>(EpisodeInfoState.Empty)

    fun interact(interaction: EpisodeInfoInteraction) {
        when (interaction) {
            EpisodeInfoInteraction.Empty -> {
            }
            is EpisodeInfoInteraction.GetEpisodeInfo -> getEpisodeInfo(interaction.episodeId)
        }
    }

    private fun getEpisodeInfo(episodeId: Int) {
        viewModelScope.launch {
            _state.value = EpisodeInfoState.Loading
            val request = repository.getEpisodeInfo(episodeId)
            if (request.isSuccessful) {
                _state.value = EpisodeInfoState.Success(request.body())
            } else {
                _state.value = EpisodeInfoState.Error
            }
        }
    }
}