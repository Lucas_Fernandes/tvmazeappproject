package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity
import com.repository.tvmazeapp.repository.FavoritesRepository
import com.repository.tvmazeapp.view.fragment.interaction.FavoritesInteraction
import com.repository.tvmazeapp.view.fragment.state.FavoritesState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(
    private val repository: FavoritesRepository
) : ViewModel() {

    val state: StateFlow<FavoritesState>
        get() = _state
    private val _state = MutableStateFlow<FavoritesState>(FavoritesState.Empty)

    fun interact(interaction: FavoritesInteraction) {
        when (interaction) {
            FavoritesInteraction.Empty -> { }
            is FavoritesInteraction.DeleteFavorite -> deleteFavorite(interaction.favorite)
            FavoritesInteraction.GetAllFavorites -> getAllFavorites()
        }
    }

    private fun getAllFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            val allFavorites = repository.getAllFavorites()
            _state.value = FavoritesState.FavoritesFromDatabase(allFavorites)
        }
    }

    private fun deleteFavorite(favoriteId: FavoriteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.delete(favoriteId)
        }
    }
}