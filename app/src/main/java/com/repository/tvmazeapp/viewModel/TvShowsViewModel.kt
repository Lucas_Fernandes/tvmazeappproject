package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.repository.TvShowsRepository
import com.repository.tvmazeapp.view.fragment.interaction.TvShowsInteraction
import com.repository.tvmazeapp.view.fragment.state.TvShowsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class TvShowsViewModel @Inject constructor(
    private val repository: TvShowsRepository
) : ViewModel() {

    val state: StateFlow<TvShowsState>
        get() = _state
    private val _state = MutableStateFlow<TvShowsState>(TvShowsState.Empty)

    fun interact(interaction: TvShowsInteraction) {
        when (interaction) {
            TvShowsInteraction.Empty -> {
            }
            TvShowsInteraction.GetAllTvShows -> getTvShows()
        }
    }

    private fun getTvShows() {
        viewModelScope.launch {
            _state.value = TvShowsState.Loading
            val request = repository.getAllTvShows()
            if (request.isSuccessful) {
                _state.value = TvShowsState.GetAllTvShowsSuccess(request.body())
            } else {
                _state.value = TvShowsState.Error
            }
        }
    }
}