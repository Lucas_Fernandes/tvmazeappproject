package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.repository.ActorsRepository
import com.repository.tvmazeapp.view.fragment.interaction.ActorsInteraction
import com.repository.tvmazeapp.view.fragment.state.ActorsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class ActorsViewModel @Inject constructor(
    private val repository: ActorsRepository
) : ViewModel() {

    val state: StateFlow<ActorsState>
        get() = _state
    private val _state = MutableStateFlow<ActorsState>(ActorsState.Empty)

    fun interact(interaction: ActorsInteraction) {
        when (interaction) {
            ActorsInteraction.Empty -> {
            }
            ActorsInteraction.GetAllActors -> getAllActors()
            is ActorsInteraction.GetActorInfoBySearch ->
                getActorInfoBySearch(interaction.actorName)
        }
    }

    private fun getAllActors() {
        viewModelScope.launch(Dispatchers.IO) {
            _state.value = ActorsState.Loading
            val request = repository.getAllPeople()
            if (request.isSuccessful) {
                _state.value = ActorsState.AllActorsSuccess(request.body())
            } else {
                _state.value = ActorsState.Error
            }
        }
    }

    private fun getActorInfoBySearch(personName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _state.value = ActorsState.Loading
            val request = repository.getActorsDetailsBySearch(personName)
            if (request.isSuccessful) {
                _state.value = ActorsState.PersonInfoSearchSuccess(request.body())
            } else {
                _state.value = ActorsState.Error
            }
        }
    }

}