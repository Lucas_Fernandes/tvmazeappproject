package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.model.remote.TvPeopleRemote
import com.repository.tvmazeapp.repository.ActorsDetailsRepository
import com.repository.tvmazeapp.view.fragment.interaction.ActorsDetailsInteraction
import com.repository.tvmazeapp.view.fragment.state.ActorsDetailsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class ActorsDetailsViewModel @Inject constructor(
    private val repository: ActorsDetailsRepository
) : ViewModel() {

    val state: StateFlow<ActorsDetailsState>
        get() = _state
    private val _state = MutableStateFlow<ActorsDetailsState>(ActorsDetailsState.Empty)

    fun interact(interaction: ActorsDetailsInteraction) {
        when (interaction) {
            ActorsDetailsInteraction.Empty -> {
            }
            is ActorsDetailsInteraction.GetActorInfo -> getActorInfo(interaction.personId)
        }
    }

    private fun getActorInfo(personId: Int) {
        viewModelScope.launch(Dispatchers.IO)
        {
            _state.value = ActorsDetailsState.Loading
            val request = repository.getPersonInfo(personId)
            if (request.isSuccessful) {
                request.body()?.let { getListOfActorsTvShows(personId, it) }
            } else {
                _state.value = ActorsDetailsState.Error
            }
        }
    }

    private fun getListOfActorsTvShows(personId: Int, personInfo: TvPeopleRemote) {
        viewModelScope.launch(Dispatchers.IO) {
            val request = repository.getListOfActorTvShows(personId)
            if(request.isSuccessful) {
                _state.apply {
                    value = ActorsDetailsState.GetListOfActorTvShows(request.body())
                    value = ActorsDetailsState.PersonInfoSuccess(personInfo)
                    value = ActorsDetailsState.Empty
                }
            } else {
                _state.apply {
                    value = ActorsDetailsState.Error
                    value = ActorsDetailsState.Empty
                }
            }
        }
    }
}