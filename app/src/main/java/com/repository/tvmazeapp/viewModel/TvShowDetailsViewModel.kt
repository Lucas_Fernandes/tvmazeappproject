package com.repository.tvmazeapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote
import com.repository.tvmazeapp.repository.FavoritesRepository
import com.repository.tvmazeapp.repository.EpisodeRepository
import com.repository.tvmazeapp.repository.TvShowsDetailsRepository
import com.repository.tvmazeapp.view.fragment.interaction.TvShowDetailsInteraction
import com.repository.tvmazeapp.view.fragment.state.TvShowsDetailsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class TvShowDetailsViewModel @Inject constructor(
    private val tvShowsDetailsRepository: TvShowsDetailsRepository,
    private val favoritesRepository: FavoritesRepository,
) : ViewModel() {

    private lateinit var lastRequestResult: TvShowDetailsRemote

    val state: StateFlow<TvShowsDetailsState>
        get() = _state
    private val _state = MutableStateFlow<TvShowsDetailsState>(TvShowsDetailsState.Empty)

    fun interact(interaction: TvShowDetailsInteraction) {
        when (interaction) {
            TvShowDetailsInteraction.Empty -> {}
            is TvShowDetailsInteraction.GetTvShowDetails -> getTvShowDetails(interaction.seasonId)
            TvShowDetailsInteraction.SaveShowAsFavorite -> saveShowAsFavorite()
            is TvShowDetailsInteraction.GetListOfSeasons -> getListOfSeasons(interaction.seasonId)
        }
    }

    private fun saveShowAsFavorite() {
        viewModelScope.launch(Dispatchers.IO) {
            val favoriteEntity = lastRequestResult.convertToDatabaseEntity(isFavorite = true)
            favoriteEntity?.let { favoritesRepository.insert(it) }
        }
    }

    private fun getTvShowDetails(seasonId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _state.value = TvShowsDetailsState.Loading
            val requestTvShowsDetails = tvShowsDetailsRepository.getTvShowDetails(seasonId)
            if (requestTvShowsDetails.isSuccessful) {
                _state.value =
                    TvShowsDetailsState.TvShowsDetailsSuccess(requestTvShowsDetails.body())
            } else {
                _state.value = TvShowsDetailsState.Error
            }
        }
    }

    private fun getListOfSeasons(seasonId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val requestListOfEpisodes = tvShowsDetailsRepository.getSeasons(seasonId)
            if (requestListOfEpisodes.isSuccessful) {
                _state.apply {
                    value = TvShowsDetailsState.ListOfSeasons(requestListOfEpisodes.body())
                }
            } else {
                _state.apply {
                    value = TvShowsDetailsState.Error
                    value = TvShowsDetailsState.Empty
                }
            }
        }
    }

//    private fun getListOfEpisodes(seasonId: Int) {
//        viewModelScope.launch(Dispatchers.IO) {
//            val requestListOfEpisodes = tvShowsDetailsRepository.getListOfEpisodes(seasonId)
//            if (requestListOfEpisodes.isSuccessful) {
//                _state.apply {
//                    value = TvShowsDetailsState.ListOfEpisodesSuccess(requestListOfEpisodes.body())
//                }
//            } else {
//                _state.apply {
//                    value = TvShowsDetailsState.Error
//                    value = TvShowsDetailsState.Empty
//                }
//            }
//        }
//    }
//
//    private suspend fun getListOfEpisodeAndHandleResults(
//        seasonId: Int,
//        tvShowDetails: TvShowDetailsRemote?
//    ) {
//        val requestListOfEpisodes = tvShowsDetailsRepository.getListOfEpisodes(seasonId)
//        if (requestListOfEpisodes.isSuccessful) {
//            _state.apply {
//                value = TvShowsDetailsState.ListOfEpisodesSuccess(requestListOfEpisodes.body())
////                value = TvShowsDetailsState.TvShowsSuccess(tvShowDetails)
//            }
//        } else {
//            _state.apply {
//                value = TvShowsDetailsState.Error
//                value = TvShowsDetailsState.Empty
//            }
//        }
//    }
}