package com.repository.tvmazeapp.di.module

import androidx.lifecycle.ViewModel
import com.repository.tvmazeapp.di.annotation.ViewModelKey
import com.repository.tvmazeapp.viewModel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ActorsViewModel::class)
    abstract fun actorsViewModel(viewModel: ActorsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesViewModel::class)
    abstract fun favoritesViewModel(viewModel: FavoritesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TvShowsViewModel::class)
    abstract fun tvShowsViewModel(viewModel: TvShowsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActorsDetailsViewModel::class)
    abstract fun actorsDetailsViewModel(viewModel: ActorsDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EpisodeInfoViewModel::class)
    abstract fun episodeInfoViewModel(viewModel: EpisodeInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesDetailsViewModel::class)
    abstract fun favoritesDetailsViewModel(viewModel: FavoritesDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TvShowDetailsViewModel::class)
    abstract fun tvShowDetailsViewModel(viewModel: TvShowDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EpisodesViewModel::class)
    abstract fun seasonsViewModel(viewModel: EpisodesViewModel): ViewModel
}