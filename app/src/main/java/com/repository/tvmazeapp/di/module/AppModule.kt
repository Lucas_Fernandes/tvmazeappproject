package com.repository.tvmazeapp.di.module

import android.app.Application
import androidx.room.Room
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.repository.tvmazeapp.R
import com.repository.tvmazeapp.application.BaseApplication
import com.repository.tvmazeapp.model.database.DATABASE_NAME
import com.repository.tvmazeapp.model.database.FavoriteDatabase
import com.repository.tvmazeapp.model.database.RoomService
import com.repository.tvmazeapp.model.database.dao.FavoriteDao
import com.repository.tvmazeapp.networking.ApiInterface
import com.repository.tvmazeapp.networking.RetrofitService
import com.repository.tvmazeapp.repository.ActorsRepository
import com.repository.tvmazeapp.repository.FavoritesRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun providesRetrofitInstance(retrofit: Retrofit): ApiInterface =
        retrofit.create(ApiInterface::class.java)

    @Singleton
    @Provides
    fun providesRetrofitService(apiInterface: ApiInterface): RetrofitService =
        RetrofitService(apiInterface)

    @Provides
    fun providesActorsRepository(retrofitService: RetrofitService): ActorsRepository =
        ActorsRepository(retrofitService)

    @Singleton
    @Provides
    fun provideRequestOptions(circularProgressDrawable: CircularProgressDrawable): RequestOptions =
        RequestOptions().placeholder(circularProgressDrawable).error(R.drawable.error_icon)

    @Singleton
    @Provides
    fun provideCircularProgressDrawable(application: Application): CircularProgressDrawable {
        return CircularProgressDrawable(application).apply {
            strokeWidth = 10f
            centerRadius = 50f
            start()
        }
    }

    @Singleton
    @Provides
    fun provideGlideInstance(
        application: Application,
        requestOptions: RequestOptions
    ): RequestManager =
        Glide.with(application).setDefaultRequestOptions(requestOptions)

    @Singleton
    @Provides
    fun providesRoomInstance(application: Application): FavoriteDatabase =
        Room.databaseBuilder(application, FavoriteDatabase::class.java, DATABASE_NAME).build()

    @Singleton
    @Provides
    fun providesFavoriteDao(database: FavoriteDatabase): FavoriteDao = database.favoriteDao()

    @Singleton
    @Provides
    fun providesFavoritesRepository(roomService: RoomService): FavoritesRepository =
        FavoritesRepository(roomService)

    @Singleton
    @Provides
    fun providesRetrofitApi(): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.tvmaze.com")
            .build()
}