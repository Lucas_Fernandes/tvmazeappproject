package com.repository.tvmazeapp.di.component

import android.app.Application
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.application.BaseApplication
import com.repository.tvmazeapp.di.module.ActivityBuilderModule
import com.repository.tvmazeapp.di.module.AppModule
import com.repository.tvmazeapp.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        AppModule::class,
    ]
)

interface AppComponent : AndroidInjector<BaseApplication> {

    fun requestManager(): RequestManager

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    override fun inject(application: BaseApplication)
}