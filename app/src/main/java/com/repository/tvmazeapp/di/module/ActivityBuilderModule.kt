package com.repository.tvmazeapp.di.module

import com.repository.tvmazeapp.view.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(
        modules = [
            FragmentBuildersModule::class,
        ]
    )

    abstract fun contributeMainActivity(): MainActivity
}