package com.repository.tvmazeapp.di.module

import com.repository.tvmazeapp.view.fragment.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeTvShowsFragment(): TvShowsFragment

    @ContributesAndroidInjector
    abstract fun contributeActorsDetailFragment(): ActorsDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeActorsActorsFragment(): ActorsFragment

    @ContributesAndroidInjector
    abstract fun contributeEpisodeInfoFragment(): EpisodeInfoFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoritesDetailsFragment(): FavoritesDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoritesFragment(): FavoritesFragment

    @ContributesAndroidInjector
    abstract fun contributeTvShowsDetailsFragment(): TvShowsDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeEpisodesFragment(): EpisodesFragment
}