package com.repository.tvmazeapp.model.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorites_table")
data class FavoriteEntity(
    @PrimaryKey
    val tvShowId: Int,
    val tvImage: String,
    val tvShowName: String,
    val isFavorite: Boolean
)
