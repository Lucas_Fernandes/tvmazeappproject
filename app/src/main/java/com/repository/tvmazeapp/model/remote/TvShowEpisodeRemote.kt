package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class TvShowEpisodeRemote(
    @SerializedName("id")
    val tvShowId: Int,
    @SerializedName("name")
    val tvShowName: String,
    @SerializedName("season")
    val tvShowSeason: Int,
    @SerializedName("number")
    val tvShowNumber: Int,
    @SerializedName("type")
    val tvShowType: String,
    @SerializedName("summary")
    val tvShowSummary: String,
    @SerializedName("image")
    val tvShowImage: ImageRemote?,
)
