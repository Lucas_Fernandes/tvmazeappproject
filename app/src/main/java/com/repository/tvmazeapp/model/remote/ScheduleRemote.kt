package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class ScheduleRemote(
    @SerializedName("time")
    val time: String,
    @SerializedName("days")
    val days: List<String>,
)