package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class ActorsDetailsRemote(
    @SerializedName("name")
    val actorName: String,
    @SerializedName("country")
    val actorCountry: ActorCountry,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("image")
    val image: ImageRemote?
)

data class ActorCountry(
    @SerializedName("name")
    val countryName: String
)
