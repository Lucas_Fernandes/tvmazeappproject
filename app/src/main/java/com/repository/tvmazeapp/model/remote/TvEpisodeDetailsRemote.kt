package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class TvEpisodeDetailsRemote(
    @SerializedName("name")
    val tvShowDetailsName: String,
    @SerializedName("number")
    val tvShowDetailsNumber: Int,
    @SerializedName("season")
    val tvShowDetailsSeason: Int,
    @SerializedName("summary")
    val tvShowDetailsSummary: String,
    @SerializedName("image")
    val imageRemote: ImageRemote?
)
