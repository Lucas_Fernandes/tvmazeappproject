package com.repository.tvmazeapp.model.database.dao

import androidx.room.*
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity

@Dao
interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(favorite: FavoriteEntity)

    @Delete
    suspend fun delete(favorite: FavoriteEntity)

    @Query("SELECT * FROM favorites_table ORDER BY tvShowName ASC")
    suspend fun getAllFavorites(): List<FavoriteEntity>

}