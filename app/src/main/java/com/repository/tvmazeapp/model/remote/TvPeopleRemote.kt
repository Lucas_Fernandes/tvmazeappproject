package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class TvPeopleRemote(
    @SerializedName("id")
    val peopleId: Int,
    @SerializedName("name")
    val peopleName: String,
    @SerializedName("image")
    val image: ImageRemote?,
)