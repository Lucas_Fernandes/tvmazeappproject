package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class TvPeopleDetailsRemote(
    @SerializedName("_embedded")
    val embedded: Embedded
)

data class Embedded(
    @SerializedName("name")
    val peopleName: String,
    @SerializedName("image")
    val image: ImageRemote?,
    @SerializedName("url")
    val url: String
)