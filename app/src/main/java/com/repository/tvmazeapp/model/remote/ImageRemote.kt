package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class ImageRemote(
    @SerializedName("medium")
    val imageSizeMedium: String,
    @SerializedName("original")
    val imageSizeOriginal: String,
)
