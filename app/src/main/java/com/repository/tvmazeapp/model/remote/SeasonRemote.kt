package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class SeasonRemote(
    @SerializedName("image")
    val image: ImageRemote?,
    @SerializedName("number")
    val seasonNumber: Int
)
