package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName

data class TvShowsRemote(
    @SerializedName("id")
    val tvShowId: Int,
    @SerializedName("image")
    val imageRemote: ImageRemote?,
    @SerializedName("name")
    val tvName: String
)