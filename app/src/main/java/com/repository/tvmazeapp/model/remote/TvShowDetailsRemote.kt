package com.repository.tvmazeapp.model.remote

import com.google.gson.annotations.SerializedName
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity

data class TvShowDetailsRemote(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val tvShowDetailsName: String,
    @SerializedName("image")
    val tvShowDetailsImage: ImageRemote?,
    @SerializedName("schedule")
    val tvShowDetailsSchedule: ScheduleRemote,
    @SerializedName("genres")
    val tvShowDetailsGenres: List<String>,
    @SerializedName("summary")
    val tvShowDetailsSummary: String,
) {
    fun convertToDatabaseEntity(isFavorite: Boolean) =
        tvShowDetailsImage?.let {
            FavoriteEntity(
                tvShowId = id,
                tvImage = it.imageSizeOriginal,
                tvShowName = tvShowDetailsName,
                isFavorite = isFavorite
            )
        }
}