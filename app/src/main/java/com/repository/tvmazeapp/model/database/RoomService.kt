package com.repository.tvmazeapp.model.database

import com.repository.tvmazeapp.model.database.dao.FavoriteDao
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity
import javax.inject.Inject

class RoomService @Inject constructor(
    private val dao: FavoriteDao
) {
    suspend fun getAllFavorites(): List<FavoriteEntity> = dao.getAllFavorites()

    suspend fun insert(favorite: FavoriteEntity) = dao.insert(favorite)

    suspend fun delete(favorite: FavoriteEntity) = dao.delete(favorite)
}