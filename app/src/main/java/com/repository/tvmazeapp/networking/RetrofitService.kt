package com.repository.tvmazeapp.networking

import javax.inject.Inject

class RetrofitService @Inject constructor(
    private val api: ApiInterface
) {
    suspend fun getAllTvShows() = api.getAllTvShows()

    suspend fun getTvShowDetails(showId: Int) = api.getTvShowDetails(showId)

    suspend fun getListOfEpisodes(seasonId: Int) = api.getListOfEpisodes(seasonId)

    suspend fun getEpisodeDetails(episodeId: Int) = api.getEpisodeDetails(episodeId)

    suspend fun getPersonInfo(personId: Int) = api.getPersonInfo(personId)

    suspend fun getAllPeople() = api.getAllPeople()

    suspend fun getListOfActorTvShows(personId: Int) = api.getListOfActorTvShows(personId)

    suspend fun getActorsSearchResult(actorName: String) = api.getActorsSearchResult(actorName)

    suspend fun getSeasons(id: Int) = api.getSeasons(id)
}