package com.repository.tvmazeapp.networking

import com.repository.tvmazeapp.model.remote.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("/shows")
    suspend fun getAllTvShows(): Response<List<TvShowsRemote>>

    @GET("/shows/{id}")
    suspend fun getTvShowDetails(@Path("id") id: Int): Response<TvShowDetailsRemote>

    @GET("/seasons/{id}/episodes")
    suspend fun getListOfEpisodes(@Path("id") id: Int): Response<List<TvShowEpisodeRemote>>

    @GET("/episodes/{id}")
    suspend fun getEpisodeDetails(@Path("id") id: Int): Response<TvEpisodeDetailsRemote>

    @GET("/people")
    suspend fun getAllPeople(): Response<List<TvPeopleRemote>>

    @GET("/people/{id}")
    suspend fun getPersonInfo(@Path("id") id: Int): Response<TvPeopleRemote>

    @GET("/people/{id}/castcredits?embed=show")
    suspend fun getListOfActorTvShows(@Path("id") id: Int): Response<List<TvPeopleDetailsRemote>>

    @GET("/search/people")
    suspend fun getActorsSearchResult(@Query("q") actorNameParam: String): Response<TvPeopleRemote>

    @GET("/shows/{id}/seasons")
    suspend fun getSeasons(@Path("id") id: Int): Response<List<SeasonRemote>>
}