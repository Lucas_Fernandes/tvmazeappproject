package com.repository.tvmazeapp.view.fragment.interaction

sealed class TvShowDetailsInteraction {
    object Empty : TvShowDetailsInteraction()
    object SaveShowAsFavorite : TvShowDetailsInteraction()
    data class GetTvShowDetails(val seasonId: Int) : TvShowDetailsInteraction()
    data class GetListOfSeasons(val seasonId: Int) : TvShowDetailsInteraction()
}
