package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.EpisodeListItemBinding
import com.repository.tvmazeapp.model.remote.TvShowEpisodeRemote

class EpisodesAdapter(
    private val layoutInflater: LayoutInflater,
    private val glide: RequestManager,
    private val clickListener: (tvShowId: Int) -> Unit
) : RecyclerView.Adapter<EpisodesAdapter.EpisodesViewHolder>() {

    private val list: MutableList<TvShowEpisodeRemote> = mutableListOf()

    fun updateList(newList: List<TvShowEpisodeRemote>) {
        list.apply {
            clear()
            addAll(newList)
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodesViewHolder =
        EpisodesViewHolder(EpisodeListItemBinding.inflate(layoutInflater, parent, false))

    override fun onBindViewHolder(holder: EpisodesViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class EpisodesViewHolder(private val binding: EpisodeListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TvShowEpisodeRemote) {
            binding.apply {
                tvEpisodePoster.text = item.tvShowName
                loadImage(ivEpisodePoster, item.tvShowImage?.imageSizeMedium)
            }

            itemView.setOnClickListener {
                clickListener(item.tvShowId)
            }
        }

        private fun loadImage(ivEpisodePoster: ImageView, imageSizeMedium: String?) {
            imageSizeMedium.let { glide.load(it).into(ivEpisodePoster) }
        }
    }
}