package com.repository.tvmazeapp.view.fragment.interaction

sealed class EpisodeInteraction {
    object Empty : EpisodeInteraction()
    data class GetEpisodes(val id: Int) : EpisodeInteraction()
}
