package com.repository.tvmazeapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentEpisodesBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.TvShowEpisodeRemote
import com.repository.tvmazeapp.view.fragment.interaction.EpisodeInteraction
import com.repository.tvmazeapp.view.fragment.state.EpisodeState
import com.repository.tvmazeapp.view.recyclerView.EpisodesAdapter
import com.repository.tvmazeapp.viewModel.EpisodesViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class EpisodesFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private val navArgs: EpisodesFragmentArgs by navArgs()
    private val viewModel: EpisodesViewModel by viewModels { viewModelFactory }
    private lateinit var binding: FragmentEpisodesBinding
    private lateinit var episodesAdapter: EpisodesAdapter
    private var tvSeasonNumber = 0
    private var tvSeasonName = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        tvSeasonNumber = navArgs.seasonNumber
        tvSeasonName = navArgs.seasonName

        viewModel.interact(EpisodeInteraction.GetEpisodes(tvSeasonNumber))
        binding = FragmentEpisodesBinding.inflate(inflater)
        observeViewModel()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        episodesAdapter = EpisodesAdapter(layoutInflater, glide) { seasonId ->
            val actions =
                EpisodesFragmentDirections.actionSeasonsFragmentToEpisodeInfoFragment(seasonId)
            findNavController().navigate(actions)
        }

        binding.apply {
            val seasonNumber = "Season $tvSeasonNumber"
            tvEpisodeSeasonName.text = tvSeasonName
            tvEpisodeSeasonNumber.text = seasonNumber

            srlEpisodes.let { srl ->
                srl.setOnRefreshListener {
                    viewModel.interact(EpisodeInteraction.GetEpisodes(tvSeasonNumber))
                    srl.isRefreshing = false
                }
            }
            rvEpisodesList.adapter = episodesAdapter
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    EpisodeState.Empty -> {}
                    EpisodeState.Error -> hideLoading()
                    is EpisodeState.Success -> updateList(state.result)
                    EpisodeState.Loading -> showLoading()
                }
            }
        }
    }

    private fun showLoading() {
        binding.apply {
            clEpisodesRoot.visibility = View.GONE
            pgSeasons.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding.apply {
            pgSeasons.visibility = View.GONE
            clEpisodesRoot.visibility = View.VISIBLE
        }
    }

    private fun updateList(list: List<TvShowEpisodeRemote>) {
        hideLoading()
        episodesAdapter.updateList(list)
        viewModel.interact(EpisodeInteraction.Empty)
    }
}