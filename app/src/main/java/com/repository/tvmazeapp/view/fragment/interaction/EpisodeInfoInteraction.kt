package com.repository.tvmazeapp.view.fragment.interaction

sealed class EpisodeInfoInteraction {
    object Empty : EpisodeInfoInteraction()
    data class GetEpisodeInfo(val episodeId: Int) : EpisodeInfoInteraction()
}
