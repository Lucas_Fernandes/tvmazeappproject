package com.repository.tvmazeapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentFavoritesDetailsBinding
import com.repository.tvmazeapp.databinding.FragmentTvShowsDetailsBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.view.fragment.interaction.FavoritesDetailsInteraction
import com.repository.tvmazeapp.viewModel.FavoritesDetailsViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FavoritesDetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private val navArgs: FavoritesDetailsFragmentArgs by navArgs()
    private lateinit var binding: FragmentTvShowsDetailsBinding
    private val viewModel: FavoritesDetailsViewModel by viewModels { viewModelFactory }
    private var favoriteId = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        favoriteId = navArgs.favoriteId
        binding = FragmentTvShowsDetailsBinding.inflate(inflater)
        viewModel.apply {
            interact(FavoritesDetailsInteraction.GetTvShowsDetails(favoriteId))
            interact(FavoritesDetailsInteraction.Empty)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}