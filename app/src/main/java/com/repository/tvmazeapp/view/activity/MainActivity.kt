package com.repository.tvmazeapp.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.repository.tvmazeapp.R
import com.repository.tvmazeapp.databinding.ActivityMainBinding
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        navController =
            (supportFragmentManager.findFragmentById(R.id.navHostFragment)
                    as NavHostFragment).navController
        setContentView(binding.root)
        setUpNavController(binding.bottomNavView, navController)
    }

    private fun setUpNavController(
        bottomNavView: BottomNavigationView,
        navController: NavController
    ) {
        NavigationUI.setupWithNavController(bottomNavView, navController)
    }
}