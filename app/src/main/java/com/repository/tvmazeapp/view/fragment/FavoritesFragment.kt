package com.repository.tvmazeapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentFavoritesBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity
import com.repository.tvmazeapp.view.fragment.interaction.FavoritesInteraction
import com.repository.tvmazeapp.view.fragment.state.FavoritesState
import com.repository.tvmazeapp.view.recyclerView.FavoritesRvAdapter
import com.repository.tvmazeapp.viewModel.FavoritesViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class FavoritesFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private lateinit var binding: FragmentFavoritesBinding
    private lateinit var adapter: FavoritesRvAdapter
    private val viewModel: FavoritesViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoritesBinding.inflate(inflater)
        viewModel.apply {
            interact(FavoritesInteraction.GetAllFavorites)
            interact(FavoritesInteraction.Empty)
        }
        observeViewModel()
        return binding.root
    }

    private fun observeViewModel() {
        viewModel.state.onEach { state ->
            when (state) {
                FavoritesState.Empty -> {
                }
                is FavoritesState.FavoritesFromDatabase -> updateList(state.favoritesList)
            }
        }
    }

    fun updateList(favoritesList: List<FavoriteEntity>) {
        adapter.updateList(favoritesList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            rvFavorites.also { rv ->
                rv.layoutManager = LinearLayoutManager(context)
                rv.adapter = FavoritesRvAdapter(layoutInflater, glide) { id ->
                    val action = FavoritesFragmentDirections
                        .actionFavoritesFragmentToFavoritesDetailsFragment(id)
                    findNavController().navigate(action)
                }
            }
        }
    }
}