package com.repository.tvmazeapp.view.fragment.interaction

sealed class FavoritesDetailsInteraction {
    object Empty : FavoritesDetailsInteraction()
    object DeleteShowFromFavorites : FavoritesDetailsInteraction()
    data class GetTvShowsDetails(val seasonId: Int) : FavoritesDetailsInteraction()
}
