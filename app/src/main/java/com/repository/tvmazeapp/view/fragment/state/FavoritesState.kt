package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.database.entity.FavoriteEntity

sealed class FavoritesState {
    object Empty : FavoritesState()
    data class FavoritesFromDatabase(val favoritesList: List<FavoriteEntity>) : FavoritesState()
}
