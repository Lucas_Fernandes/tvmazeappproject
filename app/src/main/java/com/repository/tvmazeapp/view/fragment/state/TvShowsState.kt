package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.TvShowsRemote

sealed class TvShowsState {
    object Empty : TvShowsState()
    object Loading : TvShowsState()
    object Error : TvShowsState()
    data class GetAllTvShowsSuccess(val body: List<TvShowsRemote>?) : TvShowsState()
}
