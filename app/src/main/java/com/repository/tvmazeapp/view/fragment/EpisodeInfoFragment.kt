package com.repository.tvmazeapp.view.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentEpisodeInfoBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.TvEpisodeDetailsRemote
import com.repository.tvmazeapp.view.fragment.interaction.EpisodeInfoInteraction
import com.repository.tvmazeapp.view.fragment.state.EpisodeInfoState
import com.repository.tvmazeapp.viewModel.EpisodeInfoViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class EpisodeInfoFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private val navArgs: EpisodeInfoFragmentArgs by navArgs()
    private lateinit var binding: FragmentEpisodeInfoBinding
    private val viewModel: EpisodeInfoViewModel by viewModels { viewModelFactory }
    private var tvShowId = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        tvShowId = navArgs.tvShowId
        binding = FragmentEpisodeInfoBinding.inflate(inflater)
        viewModel.apply {
            interact(EpisodeInfoInteraction.GetEpisodeInfo(tvShowId))
            interact(EpisodeInfoInteraction.Empty)
        }
        observeViewModel()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.srlTvEpisodeInfo.let { srl ->
            srl.setOnRefreshListener {
                viewModel.interact(EpisodeInfoInteraction.GetEpisodeInfo(tvShowId))
                srl.isRefreshing = false
            }
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    EpisodeInfoState.Empty -> {}
                    EpisodeInfoState.Error -> hideLoading()
                    is EpisodeInfoState.Success -> {
                        state.episodeDetails?.let {
                            hideLoading()
                            bindData(it)
                        }
                    }
                    EpisodeInfoState.Loading -> showLoading()
                }
            }
        }
    }

    private fun showLoading() {
        binding.apply {
            llEpisodeInfoRoot.visibility = View.GONE
            pgEpisodeInfo.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding.apply {
            pgEpisodeInfo.visibility = View.GONE
            llEpisodeInfoRoot.visibility = View.VISIBLE
        }
    }

    private fun bindData(episodeDetails: TvEpisodeDetailsRemote) {
        binding.apply {
            loadImage(ivEpisodeInfoPoster, episodeDetails.imageRemote?.imageSizeOriginal)
            tvEpisodeInfoName.text = extractFromHtml(episodeDetails.tvShowDetailsName)
            tvEpisodeInfoNumber.text =
                extractFromHtml(episodeDetails.tvShowDetailsNumber.toString())
            tvEpisodeInfoSeason.text = episodeDetails.tvShowDetailsSeason.toString()
            tvEpisodeInfoSummary.text = extractFromHtml(episodeDetails.tvShowDetailsSummary)
        }
    }

    private fun loadImage(image: ImageView, imageSizeOriginalUrl: String?) {
        imageSizeOriginalUrl?.let { glide.load(it).into(image) }
    }

    private fun extractFromHtml(text: String) =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString()
        else
            Html.fromHtml(text).toString()
}