package com.repository.tvmazeapp.view.fragment.interaction

sealed class ActorsInteraction {
    object Empty : ActorsInteraction()
    object GetAllActors : ActorsInteraction()
    data class GetActorInfoBySearch(val actorName: String) : ActorsInteraction()
}
