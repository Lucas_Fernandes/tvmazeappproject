package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.TvShowsListItemBinding
import com.repository.tvmazeapp.model.remote.TvShowsRemote

class TvShowsRecyclerViewAdapter(
    private val layoutInflater: LayoutInflater,
    private val glide: RequestManager,
    private val clickListener: (tvShowId: Int) -> Unit,
) : RecyclerView.Adapter<TvShowsRecyclerViewAdapter.TvShowsViewHolder>() {

    private val list: MutableList<TvShowsRemote> = mutableListOf()

    fun updateList(newList: List<TvShowsRemote>) {
        list.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowsViewHolder =
        TvShowsViewHolder(TvShowsListItemBinding.inflate(layoutInflater, parent, false))

    override fun onBindViewHolder(holder: TvShowsViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class TvShowsViewHolder(private val binding: TvShowsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TvShowsRemote) {
            binding.apply {
                loadImage(ivListItem, item.imageRemote?.imageSizeOriginal)
                tvListNameItem.text = item.tvName
            }

            itemView.setOnClickListener {
                clickListener(item.tvShowId)
            }
        }

        private fun loadImage(image: ImageView, imageSizeOriginalUrl: String?) {
            imageSizeOriginalUrl?.let { glide.load(it).into(image) }
        }
    }
}