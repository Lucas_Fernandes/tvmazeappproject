package com.repository.tvmazeapp.view.fragment.interaction

sealed class TvShowsInteraction {
    object Empty : TvShowsInteraction()
    object GetAllTvShows : TvShowsInteraction()
}
