package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.TvEpisodeDetailsRemote

sealed class EpisodeInfoState {
    data class Success(val episodeDetails: TvEpisodeDetailsRemote?) : EpisodeInfoState()
    object Empty : EpisodeInfoState()
    object Loading : EpisodeInfoState()
    object Error : EpisodeInfoState()
}
