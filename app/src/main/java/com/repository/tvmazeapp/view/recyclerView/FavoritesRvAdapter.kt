package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FavoritesListItemBinding
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity

class FavoritesRvAdapter(
    private val layoutInflater: LayoutInflater,
    private var glide: RequestManager,
    private val clickListener: (id: Int) -> Unit
) : RecyclerView.Adapter<FavoritesRvAdapter.FavoritesViewHolder>() {

    private lateinit var binding: FavoritesListItemBinding
    private var list: MutableList<FavoriteEntity> = mutableListOf()

    fun updateList(newList: List<FavoriteEntity>) {
        list.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        binding = FavoritesListItemBinding.inflate(layoutInflater)
        return FavoritesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoritesRvAdapter.FavoritesViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class FavoritesViewHolder(binding: FavoritesListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FavoriteEntity) {
            binding.apply {
                loadImage(ivFavoritesListItem, item.tvImage)
            }

            itemView.setOnClickListener {
                clickListener(item.tvShowId)
            }
        }

        private fun loadImage(image: ImageView, imageSizeOriginalUrl: String) {
            glide.load(imageSizeOriginalUrl).into(image)
        }
    }
}