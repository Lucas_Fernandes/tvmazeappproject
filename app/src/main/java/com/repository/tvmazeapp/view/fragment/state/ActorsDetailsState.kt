package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.TvPeopleDetailsRemote
import com.repository.tvmazeapp.model.remote.TvPeopleRemote

sealed class ActorsDetailsState {
    data class PersonInfoSuccess(val personInfo: TvPeopleRemote) : ActorsDetailsState()
    data class GetListOfActorTvShows(val listOfActorsTvShows: List<TvPeopleDetailsRemote>?) : ActorsDetailsState()
    object Empty : ActorsDetailsState()
    object Loading : ActorsDetailsState()
    object Error : ActorsDetailsState()
}
