package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.TvPeopleRemote

sealed class ActorsState {
    data class AllActorsSuccess(val data: List<TvPeopleRemote>?) : ActorsState()
    data class PersonInfoSearchSuccess(val data: TvPeopleRemote?) : ActorsState()
    object Error : ActorsState()
    object Empty : ActorsState()
    object Loading : ActorsState()
}
