package com.repository.tvmazeapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentTvShowsBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.TvShowsRemote
import com.repository.tvmazeapp.view.fragment.interaction.TvShowsInteraction
import com.repository.tvmazeapp.view.fragment.state.TvShowsState
import com.repository.tvmazeapp.view.recyclerView.TvShowsRecyclerViewAdapter
import com.repository.tvmazeapp.viewModel.TvShowsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class TvShowsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private lateinit var binding: FragmentTvShowsBinding
    private lateinit var adapter: TvShowsRecyclerViewAdapter
    private val viewModel: TvShowsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTvShowsBinding.inflate(inflater)
        viewModel.interact(TvShowsInteraction.GetAllTvShows)
        observeViewModel()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = TvShowsRecyclerViewAdapter(layoutInflater, glide) { id ->
            val action =
                TvShowsFragmentDirections.actionTvShowsFragmentToTvShowsDetailsFragment(id)
            findNavController().navigate(action)
        }

        binding.apply {
            svTvShows

            rvTvShows.also { rv ->
                rv.layoutManager = GridLayoutManager(context, 2)
                rv.adapter = adapter
            }

            srlTvShows.let { srl ->
                srl.setOnRefreshListener {
                    viewModel.interact(TvShowsInteraction.GetAllTvShows)
                    srl.isRefreshing = false
                }
            }
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    TvShowsState.Empty -> {
                    }
                    TvShowsState.Error -> hideLoading()
                    is TvShowsState.GetAllTvShowsSuccess -> state.body?.let { r -> updateList(r) }
                    TvShowsState.Loading -> showLoading()
                }
            }
        }
    }

    private fun showLoading() {
        binding.run {
            tvShowsRootLayout.visibility = View.GONE
            pgTvShows.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding.run {
            tvShowsRootLayout.visibility = View.VISIBLE
            binding.pgTvShows.visibility = View.GONE
        }
    }

    private fun updateList(response: List<TvShowsRemote>) {
        hideLoading()
        adapter.updateList(response)
    }
}