package com.repository.tvmazeapp.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentActorDetailsBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.TvPeopleDetailsRemote
import com.repository.tvmazeapp.view.fragment.interaction.ActorsDetailsInteraction
import com.repository.tvmazeapp.view.fragment.state.ActorsDetailsState
import com.repository.tvmazeapp.view.recyclerView.ActorsDetailsTvShowsListAdapter
import com.repository.tvmazeapp.viewModel.ActorsDetailsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class ActorsDetailFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private val navArgs: ActorsDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentActorDetailsBinding
    private val viewModel: ActorsDetailsViewModel by viewModels { viewModelFactory }
    private lateinit var tvShowsListAdapter: ActorsDetailsTvShowsListAdapter
    private var personId = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        personId = navArgs.personId
        binding = FragmentActorDetailsBinding.inflate(inflater)
        viewModel.apply {
            interact(ActorsDetailsInteraction.GetActorInfo(personId))
            interact(ActorsDetailsInteraction.Empty)
        }
        observeViewModel()
        return binding.root
    }

    private fun observeViewModel() {
        viewModel.state.onEach { state ->
            when (state) {
                ActorsDetailsState.Empty -> {
                }
                ActorsDetailsState.Error -> hideLoading()
                ActorsDetailsState.Loading -> showLoading()
                is ActorsDetailsState.PersonInfoSuccess -> state.personInfo
                is ActorsDetailsState.GetListOfActorTvShows ->
                    state.listOfActorsTvShows?.let { updateList(it) }
            }
        }
    }

    private fun showLoading() {
        binding.pgTvShows.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.pgTvShows.visibility = View.GONE
    }

    private fun updateList(listOfActorsTvShows: List<TvPeopleDetailsRemote>) {
        hideLoading()
        tvShowsListAdapter.updateList(listOfActorsTvShows)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvShowsListAdapter = ActorsDetailsTvShowsListAdapter(glide, layoutInflater) { url ->
                val intent = Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(url) }
                startActivity(intent)
            }

            srlTvShowsDetails.let { srl ->
                srl.setOnRefreshListener {
                    showLoading()
                    viewModel.interact(ActorsDetailsInteraction.GetActorInfo(personId))
                    srl.isRefreshing = false
                }
            }
        }
    }
}