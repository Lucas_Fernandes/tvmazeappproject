package com.repository.tvmazeapp.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentActorsBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.TvPeopleRemote
import com.repository.tvmazeapp.view.fragment.interaction.ActorsInteraction
import com.repository.tvmazeapp.view.fragment.state.ActorsState
import com.repository.tvmazeapp.view.recyclerView.ActorsRvAdapter
import com.repository.tvmazeapp.viewModel.ActorsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class ActorsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private lateinit var binding: FragmentActorsBinding
    private lateinit var adapter: ActorsRvAdapter
    private val viewModel: ActorsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentActorsBinding.inflate(inflater)
        viewModel.apply {
            interact(ActorsInteraction.GetAllActors)
            interact(ActorsInteraction.Empty)
        }

        observeViewModel()
        return binding.root
    }

    private fun observeViewModel() {
        viewModel.state.onEach { state ->
            when (state) {
                is ActorsState.AllActorsSuccess -> state.data?.let { updateList(it) }
                ActorsState.Empty -> {
                }
                ActorsState.Error -> hideLoading()
                ActorsState.Loading -> showLoading()
                is ActorsState.PersonInfoSearchSuccess -> state.data?.let { updateList(listOf(it)) }
            }
        }
    }

    private fun updateList(data: List<TvPeopleRemote>) {
        hideLoading()
        adapter.updateList(data)
    }

    private fun showLoading() {
        binding.pgActors.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.pgActors.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ActorsRvAdapter(layoutInflater, glide) { id ->
            val action = ActorsFragmentDirections.actionActorsFragmentToActorsDetailsFragment(id)
            findNavController().navigate(action)
        }

        binding.apply {
            srlActors.also { srl ->
                srl.setOnRefreshListener {
                    viewModel.interact(ActorsInteraction.GetAllActors)
                    srl.isRefreshing = false
                }
            }
        }
    }
}