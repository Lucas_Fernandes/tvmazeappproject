package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.TvShowEpisodeRemote

sealed class EpisodeState {
    object Empty : EpisodeState()
    object Error : EpisodeState()
    object Loading : EpisodeState()
    data class Success(val result: List<TvShowEpisodeRemote>) : EpisodeState()
}

