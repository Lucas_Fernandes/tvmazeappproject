package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.SeasonRemote
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote

sealed class FavoritesDetailsState {
    object Empty : FavoritesDetailsState()
    object Error : FavoritesDetailsState()
    object Loading : FavoritesDetailsState()
    data class TvShowsSuccess(val tvShowDetailsResult: TvShowDetailsRemote?) :
        FavoritesDetailsState()

    data class ListOfSeasonsSuccess(val listOfSeasonsResult: List<SeasonRemote>?) :
        FavoritesDetailsState()
}
