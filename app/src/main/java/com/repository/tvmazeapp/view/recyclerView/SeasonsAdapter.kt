package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.SeasonsListItemBinding
import com.repository.tvmazeapp.model.remote.SeasonRemote

class SeasonsAdapter(
    private val layoutInflater: LayoutInflater,
    private val glide: RequestManager,
    private val clickListener: (item: SeasonRemote) -> Unit
) : RecyclerView.Adapter<SeasonsAdapter.SeasonsViewHolder>() {

    private val list: MutableList<SeasonRemote> = mutableListOf()

    fun updateList(newList: List<SeasonRemote>) {
        list.apply {
            clear()
            addAll(newList)
        }

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonsViewHolder =
        SeasonsViewHolder(SeasonsListItemBinding.inflate(layoutInflater, parent, false))

    override fun onBindViewHolder(holder: SeasonsViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class SeasonsViewHolder(private val binding: SeasonsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SeasonRemote) {
            binding.apply {
                val itemText = "Season ${item.seasonNumber}"
                tvSeasonNumber.text = itemText
                loadImage(ivSeasonPoster, item.image?.imageSizeMedium)
            }

            itemView.setOnClickListener {
                clickListener(item)
            }
        }
    }

    private fun loadImage(image: ImageView, imageSizeOriginalUrl: String?) {
        imageSizeOriginalUrl?.let { glide.load(it).into(image) }
    }
}