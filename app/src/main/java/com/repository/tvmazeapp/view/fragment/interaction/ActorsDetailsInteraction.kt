package com.repository.tvmazeapp.view.fragment.interaction

sealed class ActorsDetailsInteraction {
    object Empty : ActorsDetailsInteraction()
    data class GetActorInfo(val personId: Int) : ActorsDetailsInteraction()
}
