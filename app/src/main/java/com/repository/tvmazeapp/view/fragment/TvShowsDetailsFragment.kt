package com.repository.tvmazeapp.view.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.FragmentTvShowsDetailsBinding
import com.repository.tvmazeapp.di.factory.ViewModelFactory
import com.repository.tvmazeapp.model.remote.SeasonRemote
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote
import com.repository.tvmazeapp.view.fragment.interaction.TvShowDetailsInteraction
import com.repository.tvmazeapp.view.fragment.state.TvShowsDetailsState
import com.repository.tvmazeapp.view.recyclerView.SeasonsAdapter
import com.repository.tvmazeapp.viewModel.TvShowDetailsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class TvShowsDetailsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var glide: RequestManager

    private lateinit var seasonsAdapter: SeasonsAdapter
    private val navArgs: TvShowsDetailsFragmentArgs by navArgs()
    private lateinit var binding: FragmentTvShowsDetailsBinding
    private val viewModel: TvShowDetailsViewModel by viewModels { viewModelFactory }
    private var tvShowId = 0
    private var seasonName = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        tvShowId = navArgs.tvShowId
        binding = FragmentTvShowsDetailsBinding.inflate(inflater)
        viewModel.apply { interact(TvShowDetailsInteraction.GetTvShowDetails(tvShowId)) }
        observeViewModel()
        return binding.root
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.collect { state ->
                when (state) {
                    TvShowsDetailsState.Empty -> {}
                    TvShowsDetailsState.Error -> hideLoading()
                    is TvShowsDetailsState.ListOfSeasons ->
                        state.listOfSeasons?.let { updateList(it) }
                    TvShowsDetailsState.Loading -> showLoading()
                    is TvShowsDetailsState.TvShowsDetailsSuccess -> {
                        state.tvShowsDetails?.let { bindData(it) }
                        viewModel.interact(TvShowDetailsInteraction.GetListOfSeasons(tvShowId))
                    }
                }
            }
        }
    }

    private fun bindData(result: TvShowDetailsRemote) {
        hideLoading()
        seasonName = result.tvShowDetailsName
        binding.apply {
            result.tvShowDetailsImage?.imageSizeMedium?.let { loadImage(ivEpisodeInfoPoster, it) }
            tvShowDetailsName.text = result.tvShowDetailsName
            tvShowDetailsSummary.text = extractFromHtml(result.tvShowDetailsSummary)
            tvShowDetailsSchedule.text = buildScheduleString(result)
            tvShowsDetailsSummary.text = buildGenresString(result.tvShowDetailsGenres)
        }
    }

    private fun buildGenresString(genresList: List<String>): String {
        return genresList.joinToString(separator = " | ", postfix = "")
    }

    private fun buildScheduleString(result: TvShowDetailsRemote): String {
        val time = result.tvShowDetailsSchedule.time
        val day = result.tvShowDetailsSchedule.days[0]
        return "$day at $time"
    }


    private fun loadImage(image: ImageView, imageSizeOriginalUrl: String) {
        glide.load(imageSizeOriginalUrl).into(image)
    }

    private fun showLoading() {
        binding.apply {
            llTvShowDetailsRoot.visibility = View.GONE
            pgTvShows.visibility = View.VISIBLE
        }
    }

    private fun hideLoading() {
        binding.apply {
            pgTvShows.visibility = View.GONE
            llTvShowDetailsRoot.visibility = View.VISIBLE
        }
    }

    private fun updateList(list: List<SeasonRemote>) {
        hideLoading()
        seasonsAdapter.updateList(list)
        viewModel.interact(TvShowDetailsInteraction.Empty)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvShowDetailsName

            srlTvShowsDetails.let { srl ->
                srl.setOnRefreshListener {
                    viewModel.interact(TvShowDetailsInteraction.GetTvShowDetails(tvShowId))
                    srl.isRefreshing = false
                }
            }

            seasonsAdapter = SeasonsAdapter(layoutInflater, glide) { item ->
                val action =
                    TvShowsDetailsFragmentDirections.actionTvShowsDetailsFragmentToEpisodesFragment(
                        seasonNumber = item.seasonNumber,
                        seasonName = seasonName
                    )
                findNavController().navigate(action)
            }

            rvShowsSeasonsList.adapter = seasonsAdapter
        }
    }

    private fun extractFromHtml(text: String) =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString()
        else
            Html.fromHtml(text).toString()
}