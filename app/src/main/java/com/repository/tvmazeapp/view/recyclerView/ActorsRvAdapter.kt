package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.ActorsListItemBinding
import com.repository.tvmazeapp.model.remote.TvPeopleRemote

class ActorsRvAdapter(
    private val layoutInflater: LayoutInflater,
    private val glide: RequestManager,
    private val clickListener: (id: Int) -> Unit
) : RecyclerView.Adapter<ActorsRvAdapter.ActorsViewHolder>() {

    private lateinit var binding: ActorsListItemBinding
    private var list: MutableList<TvPeopleRemote> = mutableListOf()

    fun updateList(newList: List<TvPeopleRemote>) {
        list.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActorsViewHolder {
        binding = ActorsListItemBinding.inflate(layoutInflater)
        return ActorsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ActorsViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class ActorsViewHolder(binding: ActorsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TvPeopleRemote) {
            loadImage(binding.ivActorsListItem, item.image?.imageSizeOriginal)

            itemView.setOnClickListener {
                clickListener(item.peopleId)
            }
        }
    }

    private fun loadImage(image: ImageView, imageSizeOriginalUrl: String?) {
        imageSizeOriginalUrl?.let { glide.load(it).into(image) }
    }
}