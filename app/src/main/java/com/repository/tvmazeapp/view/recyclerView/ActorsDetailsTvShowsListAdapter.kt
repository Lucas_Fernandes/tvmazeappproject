package com.repository.tvmazeapp.view.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.repository.tvmazeapp.databinding.ActorDetailsListItemBinding
import com.repository.tvmazeapp.model.remote.TvPeopleDetailsRemote

class ActorsDetailsTvShowsListAdapter(
    private val glide: RequestManager,
    private val layoutInflater: LayoutInflater,
    private val clickListener: (url: String) -> Unit
) : RecyclerView.Adapter<ActorsDetailsTvShowsListAdapter.ActorsDetailsViewHolder>() {
    private lateinit var binding: ActorDetailsListItemBinding
    private val list: MutableList<TvPeopleDetailsRemote> = mutableListOf()

    fun updateList(newList: List<TvPeopleDetailsRemote>) {
        list.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActorsDetailsViewHolder {
        binding = ActorDetailsListItemBinding.inflate(layoutInflater)
        return ActorsDetailsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ActorsDetailsViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size

    inner class ActorsDetailsViewHolder(binding: ActorDetailsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TvPeopleDetailsRemote) {
            binding.apply {
                loadImage(ivEpisodeInfoActorDetails, item.embedded.image?.imageSizeOriginal)
            }

            itemView.setOnClickListener {
                clickListener(item.embedded.url)
            }
        }

        private fun loadImage(image: ImageView, imageSizeOriginalUrl: String?) {
            imageSizeOriginalUrl?.let { glide.load(it).into(image) }
        }
    }
}