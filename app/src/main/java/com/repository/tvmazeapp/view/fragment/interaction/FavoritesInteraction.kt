package com.repository.tvmazeapp.view.fragment.interaction

import com.repository.tvmazeapp.model.database.entity.FavoriteEntity

sealed class FavoritesInteraction {
    object Empty :FavoritesInteraction()
    object GetAllFavorites :FavoritesInteraction()
    data class DeleteFavorite(val favorite: FavoriteEntity) :FavoritesInteraction()
}
