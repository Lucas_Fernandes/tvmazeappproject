package com.repository.tvmazeapp.view.fragment.state

import com.repository.tvmazeapp.model.remote.SeasonRemote
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote

sealed class TvShowsDetailsState {
    object Empty : TvShowsDetailsState()
    object Error : TvShowsDetailsState()
    object Loading : TvShowsDetailsState()
    data class TvShowsDetailsSuccess(val tvShowsDetails: TvShowDetailsRemote?) :
        TvShowsDetailsState()

    data class ListOfSeasons(val listOfSeasons: List<SeasonRemote>?) :
        TvShowsDetailsState()
}
