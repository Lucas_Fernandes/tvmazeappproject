package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.TvShowsRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class TvShowsRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getAllTvShows(): Response<List<TvShowsRemote>> = service.getAllTvShows()
}