package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.SeasonRemote
import com.repository.tvmazeapp.model.remote.TvShowEpisodeRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class EpisodeRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getListOfEpisodes(seasonId: Int): Response<List<TvShowEpisodeRemote>> =
        service.getListOfEpisodes(seasonId)
}