package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.TvPeopleDetailsRemote
import com.repository.tvmazeapp.model.remote.TvPeopleRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class ActorsDetailsRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getPersonInfo(personId: Int): Response<TvPeopleRemote> =
        service.getPersonInfo(personId)

    suspend fun getListOfActorTvShows(personId: Int): Response<List<TvPeopleDetailsRemote>> =
        service.getListOfActorTvShows(personId)
}