package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.TvPeopleRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class ActorsRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getActorsDetailsBySearch(actorsName: String): Response<TvPeopleRemote> =
        service.getActorsSearchResult(actorsName)

    suspend fun getAllPeople(): Response<List<TvPeopleRemote>> = service.getAllPeople()
}