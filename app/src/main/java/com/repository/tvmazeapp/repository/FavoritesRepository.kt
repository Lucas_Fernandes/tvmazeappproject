package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.database.RoomService
import com.repository.tvmazeapp.model.database.entity.FavoriteEntity
import javax.inject.Inject

class FavoritesRepository @Inject constructor(
    private val roomService: RoomService
) {

    suspend fun getAllFavorites(): List<FavoriteEntity> = roomService.getAllFavorites()

    suspend fun insert(favorite: FavoriteEntity) = roomService.insert(favorite)

    suspend fun delete(favorite: FavoriteEntity) = roomService.delete(favorite)
}