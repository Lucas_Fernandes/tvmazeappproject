package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.SeasonRemote
import com.repository.tvmazeapp.model.remote.TvShowEpisodeRemote
import com.repository.tvmazeapp.model.remote.TvShowDetailsRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class TvShowsDetailsRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getTvShowDetails(showId: Int): Response<TvShowDetailsRemote> =
        service.getTvShowDetails(showId)

    suspend fun getSeasons(seasonId: Int): Response<List<SeasonRemote>> =
        service.getSeasons(seasonId)

}