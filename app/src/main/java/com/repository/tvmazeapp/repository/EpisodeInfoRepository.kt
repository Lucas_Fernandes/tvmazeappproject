package com.repository.tvmazeapp.repository

import com.repository.tvmazeapp.model.remote.TvEpisodeDetailsRemote
import com.repository.tvmazeapp.networking.RetrofitService
import retrofit2.Response
import javax.inject.Inject

class EpisodeInfoRepository @Inject constructor(
    private val service: RetrofitService
) {
    suspend fun getEpisodeInfo(episodeId: Int): Response<TvEpisodeDetailsRemote> =
        service.getEpisodeDetails(episodeId)
}